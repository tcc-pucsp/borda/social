package br.com.projeto.pucsp.tcc.social.rabbitmq;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import br.com.projeto.pucsp.tcc.social.excecao.EnderecoNaoEncontradoException;
import br.com.projeto.pucsp.tcc.social.modelo.GeolocalizacaoRecebimento;
import br.com.projeto.pucsp.tcc.social.observador.implementacao.SocialObservador;
import io.micrometer.core.instrument.util.IOUtils;

@ActiveProfiles( value = "test" )
@RunWith( SpringRunner.class )
@SpringBootTest
@AutoConfigureMockMvc
@FixMethodOrder( MethodSorters.NAME_ASCENDING )
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
public class RabbitMQTest {
	
	private static final String ENDPOINT_SOCIAL = "/social/";
	
	private static final String ID_JSON = "$.id";
	private static final Integer ID = 1;
	
	@Autowired
	private SocialObservador socialObservador;
	
	@Autowired
	private MockMvc mockMvc;

	private final ClassLoader classLoader = getClass().getClassLoader();
	
	@Test
	public void cadastrarSocialComSucessoTest() throws Exception {

		try (InputStream inputStream = classLoader.getResourceAsStream("social.json")) {

			String result = IOUtils.toString(Objects.requireNonNull(inputStream), StandardCharsets.UTF_8);

			mockMvc.perform(MockMvcRequestBuilders
					.post( ENDPOINT_SOCIAL )
					.contentType( MediaType.APPLICATION_JSON_UTF8 )
					.content( result ))
			.andDo( MockMvcResultHandlers.print() )
			.andExpect( MockMvcResultMatchers.status().isCreated() )
			.andExpect( MockMvcResultMatchers.content().contentType( MediaType.APPLICATION_JSON_UTF8_VALUE ) )
			.andExpect( MockMvcResultMatchers.jsonPath( ID_JSON ).value( ID ) );

		}


	}
	
	@Test
	public void processarEnderecoIdExistente() {

		GeolocalizacaoRecebimento geolocalizacaoRecebimento = new GeolocalizacaoRecebimento();
		
		geolocalizacaoRecebimento.setId(1);
		geolocalizacaoRecebimento.setEndereco("247, Rua Caio Prado, República, São Paulo, Sao Paulo, São Paulo, Brazil, 01303-001");
		geolocalizacaoRecebimento.setLatitude(-23.549843);
		geolocalizacaoRecebimento.setLatitude(-46.648216);

		socialObservador.processar( geolocalizacaoRecebimento );
		
	}
	
	@Test(expected = EnderecoNaoEncontradoException.class)
	public void processarEnderecoIdInexistente() {

		GeolocalizacaoRecebimento geolocalizacaoRecebimento = new GeolocalizacaoRecebimento();
		
		geolocalizacaoRecebimento.setId(2);
		geolocalizacaoRecebimento.setEndereco("247, Rua Caio Prado, República, São Paulo, Sao Paulo, São Paulo, Brazil, 01303-001");
		geolocalizacaoRecebimento.setLatitude(-23.549843);
		geolocalizacaoRecebimento.setLatitude(-46.648216);

		socialObservador.processar( geolocalizacaoRecebimento );


	}


}
