package br.com.projeto.pucsp.tcc.social;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import br.com.projeto.pucsp.tcc.social.enumeracao.Notificacao;
import io.micrometer.core.instrument.util.IOUtils;

@ActiveProfiles( value = "test" )
@RunWith( SpringRunner.class )
@SpringBootTest
@AutoConfigureMockMvc
@FixMethodOrder( MethodSorters.NAME_ASCENDING )
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
public class ControleLoginTest {

	private static final String ENDPOINT_SOCIAL = "/social/";
	private static final String ENDPOINT_SOCIAL_LOGIN = "/social/login/";
	
	private static final String CODIGO_ERRO = "$.notificacao.codigo";
	private static final String PROTOCOLO = "$.notificacao.contexto";

	private static final String EMAIL_JSON = "$.email";
	private static final String ID_JSON = "$.id";
	private static final String CNPJ_JSON = "$.cnpj.registro";
	
	private static final String EMAIL = "email@dominio.com";
	private static final Integer ID = 1;
	private static final String CNPJ = "20112441000192";
	
	@Autowired
	private MockMvc mockMvc;

	private final ClassLoader classLoader = getClass().getClassLoader();
	
	@Test
	public void cadastrarSocialComSucessoTest() throws Exception {

		try (InputStream inputStream = classLoader.getResourceAsStream("social.json")) {

			String result = IOUtils.toString(Objects.requireNonNull(inputStream), StandardCharsets.UTF_8);

			mockMvc.perform(MockMvcRequestBuilders
					.post( ENDPOINT_SOCIAL )
					.contentType( MediaType.APPLICATION_JSON_UTF8 )
					.content( result ))
			.andDo( MockMvcResultHandlers.print() )
			.andExpect( MockMvcResultMatchers.status().isCreated() )
			.andExpect( MockMvcResultMatchers.content().contentType( MediaType.APPLICATION_JSON_UTF8_VALUE ) )
			.andExpect( MockMvcResultMatchers.jsonPath( ID_JSON ).value( ID ) );

		}


	}
	
	@Test
	public void loginSocialTest() throws Exception {
		
		final String cnpj = "20112441000192";
		
		mockMvc.perform(MockMvcRequestBuilders
                .get( ENDPOINT_SOCIAL_LOGIN + cnpj))
        .andDo( MockMvcResultHandlers.print() )
        .andExpect( MockMvcResultMatchers.status().isOk() )
        .andExpect( MockMvcResultMatchers.content().contentType( MediaType.APPLICATION_JSON_UTF8_VALUE ) )
        .andExpect( MockMvcResultMatchers.jsonPath( ID_JSON ).value( ID ) )
        .andExpect( MockMvcResultMatchers.jsonPath( EMAIL_JSON ).value( EMAIL ) )
        .andExpect( MockMvcResultMatchers.jsonPath( CNPJ_JSON ).value( CNPJ ) );
		
	}
	
	@Test
	public void loginSocialInexistenteTest() throws Exception {
		
		final String cnpj = "12100469000181";

		mockMvc.perform(MockMvcRequestBuilders
                .get( ENDPOINT_SOCIAL_LOGIN + cnpj))
        .andDo( MockMvcResultHandlers.print() )
        .andExpect( MockMvcResultMatchers.status().isNotFound() )
        .andExpect( MockMvcResultMatchers.content().contentType( MediaType.APPLICATION_JSON_UTF8_VALUE ) )
        .andExpect( MockMvcResultMatchers.jsonPath( CODIGO_ERRO ).value( Notificacao.SOCIAL_NAO_ENCONTRADO.getCodigo() ) )
        .andExpect( MockMvcResultMatchers.jsonPath( PROTOCOLO ).value( Notificacao.SOCIAL_NAO_ENCONTRADO.getContexto() ) );
		
	}
	
}
