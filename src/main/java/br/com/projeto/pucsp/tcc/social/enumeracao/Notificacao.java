package br.com.projeto.pucsp.tcc.social.enumeracao;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor( access = AccessLevel.PRIVATE )
@Getter
@JsonFormat( shape = JsonFormat.Shape.OBJECT )
public enum Notificacao {

	SOCIAL_JA_CADASTRADO( 409, "SOCIAL JA CADASTRADO" ),
	
	ENDERECO_NAO_ENCONTRADO( 404, "ENDERECO NAO ENCONTRADO" ),
	
	SOCIAL_NAO_ENCONTRADO( 404, "SOCIAL NAO ENCONTRADO"),
	
	DADOS_INVALIDOS( 400, "DADOS INVALIDOS");
	
	private final Integer codigo;
	
	private final String contexto;
	
}
