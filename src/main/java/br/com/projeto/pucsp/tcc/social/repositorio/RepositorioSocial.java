package br.com.projeto.pucsp.tcc.social.repositorio;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.projeto.pucsp.tcc.social.modelo.Social;

public interface RepositorioSocial extends JpaRepository<Social, Integer>{
	
	<T> Optional<T> findSocialByCnpjRegistroAndDataEncerramentoIsNull( String cnpj, Class<T> type );
	
	Optional<Social> findSocialByCnpjRegistro( String cnpj );
	
	Optional<Social> findSocialByEnderecoId( Integer id );
	
}
