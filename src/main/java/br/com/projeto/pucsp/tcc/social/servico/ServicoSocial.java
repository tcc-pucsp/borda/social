package br.com.projeto.pucsp.tcc.social.servico;

import org.springframework.stereotype.Service;

import br.com.projeto.pucsp.tcc.social.cliente.ClienteGeolocalizacao;
import br.com.projeto.pucsp.tcc.social.dto.SocialDto;
import br.com.projeto.pucsp.tcc.social.excecao.SocialNaoEncontradoException;
import br.com.projeto.pucsp.tcc.social.modelo.EnderecoPublicacao;
import br.com.projeto.pucsp.tcc.social.modelo.IdSocial;
import br.com.projeto.pucsp.tcc.social.modelo.Social;
import br.com.projeto.pucsp.tcc.social.proxy.SocialProxy;
import br.com.projeto.pucsp.tcc.social.recurso.SocialRecurso;
import br.com.projeto.pucsp.tcc.social.repositorio.RepositorioSocial;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ServicoSocial implements SocialRecurso {

	private final RepositorioSocial repositorio;
	
	private final ServicoCadastrar servicoCadastrar;
	
	private final ClienteGeolocalizacao clienteGeolocalizacao;
	
	public ServicoSocial( RepositorioSocial repositorio, 
						  ServicoCadastrar servicoCadastrar,
						  ClienteGeolocalizacao clienteGeolocalizacao) {
		
		this.repositorio = repositorio;
		
		this.servicoCadastrar = servicoCadastrar;
		
		this.clienteGeolocalizacao = clienteGeolocalizacao;
		
	}

	@Override
	public IdSocial cadastrarSocial( SocialDto socialDto ) {	
		
		Social social = servicoCadastrar.processar( socialDto );
		
		EnderecoPublicacao enderecoPublicacao = new EnderecoPublicacao( social.getEndereco() );
		
		clienteGeolocalizacao.publicar( enderecoPublicacao );
		
		return new IdSocial( social.getId() );
	}
	
	@Override
	public SocialProxy obterSocial( String cnpj ) {
		
		final SocialProxy socialProxy = repositorio.findSocialByCnpjRegistroAndDataEncerramentoIsNull( cnpj, SocialProxy.class )
				.orElseThrow( () -> new SocialNaoEncontradoException( "" ) );
		
		log.info("Obtendo social {}", socialProxy.getId());
		
		return socialProxy;
		
	}
	
	@Override
	public IdSocial encerrarSocial( Integer id ) {
		
		final Social social = repositorio.findById( id ).orElseThrow( () -> new SocialNaoEncontradoException( id ) );
		
 		if( social.encerrado() ) {
 			throw new SocialNaoEncontradoException( id );
 		}
 		
 		social.encerrar();
		
		repositorio.save(social);
		
		log.info("Encerrando acesso ao social com cnpj {}", id);
		
		return new IdSocial( social.getId() );
		
	}
	
	@Override
	public IdSocial atualizarSocial( Integer id, SocialDto socialDto ) {
		
		Social socialCadastrado = repositorio.findById( id ).orElseThrow( () -> new SocialNaoEncontradoException( id ) );

		if( socialCadastrado.encerrado() ) {
			throw new SocialNaoEncontradoException( socialCadastrado.getId() );
		}
		
		socialCadastrado.atualizar( socialDto );
		
		Social social = repositorio.save( socialCadastrado );
		
		EnderecoPublicacao enderecoPublicacao = new EnderecoPublicacao( social.getEndereco() );
		
		clienteGeolocalizacao.publicar( enderecoPublicacao );
		
		log.info("Social {} atualizado", social.getId());
		
		return new IdSocial( social.getId() );
		
	}
	
}
