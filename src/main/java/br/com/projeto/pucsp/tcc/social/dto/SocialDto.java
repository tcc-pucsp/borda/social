package br.com.projeto.pucsp.tcc.social.dto;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Value
@Getter
public final class SocialDto {

	@NotBlank
	private final String nomeInstituicao;
	
	@NotBlank
	@Size( max = 15 )
	private String telefone;
	
	@Email
	@NotBlank
	private final String email;
	
	@NotBlank
	private final String senha;
	
	@NotNull
	@Valid
	private final EnderecoDto endereco;
	
	@NotNull
	@Valid
	private final CnpjDto cnpj;

	public String getSenha() {
		return new BCryptPasswordEncoder().encode(senha);
	}
}
