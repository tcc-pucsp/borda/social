package br.com.projeto.pucsp.tcc.social.modelo;

import lombok.Value;

@Value
public class IdSocial {

	Integer id;
	
}
