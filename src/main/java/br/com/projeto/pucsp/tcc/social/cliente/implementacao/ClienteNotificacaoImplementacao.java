package br.com.projeto.pucsp.tcc.social.cliente.implementacao;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

import br.com.projeto.pucsp.tcc.social.cliente.ClienteNotificacao;
import br.com.projeto.pucsp.tcc.social.modelo.EmailPublicacao;
import br.com.projeto.pucsp.tcc.social.propriedade.PropriedadeNotificacao;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class ClienteNotificacaoImplementacao implements ClienteNotificacao {

	private final RabbitTemplate rabbitTemplate;
	
	private final PropriedadeNotificacao propriedadeNotificacao;
	
	public ClienteNotificacaoImplementacao( RabbitTemplate rabbitTemplate, PropriedadeNotificacao propriedadeNotificacao ) {
		
		this.rabbitTemplate = rabbitTemplate;
		
		this.propriedadeNotificacao = propriedadeNotificacao;
		
	}
	
	@Override
	public void publicar( EmailPublicacao emailPublicacao ) {
		
		rabbitTemplate.convertAndSend( propriedadeNotificacao.getTopicExchangeNotificacao(), emailPublicacao );
		
		log.info("Mensagem Publicada com Sucesso no RabbitMQ");
	}
	
}
