package br.com.projeto.pucsp.tcc.social.proxy;

public interface SocialProxy {
	
	Integer getId();
	
	String getNomeInstituicao();
	
	String getEmail();
	
	EnderecoProxy getEndereco();
	
	CnpjProxy getCnpj();
	
	Boolean getAtivo();
	
	interface EnderecoProxy{
		
		String getCep();
		
		String getLogradouro();
		
		String getComplemento();
		
		String getCidade();
		
		String getEstado();
		
		Integer getNumero();
		
		Double getLatitude();
		
		Double getLongitude();
		
	}
	
	interface CnpjProxy{
		
		String getRegistro();
		
	}

}
