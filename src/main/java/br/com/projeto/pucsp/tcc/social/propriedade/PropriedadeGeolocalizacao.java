package br.com.projeto.pucsp.tcc.social.propriedade;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Data;

@Data
@ConfigurationProperties( prefix = "rabbit.social.geolocalizacao" )
public class PropriedadeGeolocalizacao {

	private String topicExchange;
	
}
