package br.com.projeto.pucsp.tcc.social.recurso;

import br.com.projeto.pucsp.tcc.social.proxy.LoginProxy;

public interface LoginRecurso {

	LoginProxy login( String cnpj );
	
}
