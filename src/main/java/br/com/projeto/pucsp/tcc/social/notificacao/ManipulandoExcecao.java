package br.com.projeto.pucsp.tcc.social.notificacao;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import br.com.projeto.pucsp.tcc.social.enumeracao.Notificacao;
import br.com.projeto.pucsp.tcc.social.excecao.EnderecoNaoEncontradoException;
import br.com.projeto.pucsp.tcc.social.excecao.SocialJaCadastradoException;
import br.com.projeto.pucsp.tcc.social.excecao.SocialNaoEncontradoException;
import br.com.projeto.pucsp.tcc.social.modelo.Resposta;
import lombok.extern.slf4j.Slf4j;

@RestControllerAdvice
@Slf4j
public class ManipulandoExcecao {
	
	@ResponseStatus( HttpStatus.BAD_REQUEST )
    @ExceptionHandler( {MethodArgumentNotValidException.class} )
    public Resposta methodsArgumentsInvalidos( MethodArgumentNotValidException e ){

        log.info("Dados Invalidos");

        return new Resposta( Notificacao.DADOS_INVALIDOS, e.getMessage());

    }
	
	@ResponseStatus( HttpStatus.CONFLICT )
    @ExceptionHandler( {SocialJaCadastradoException.class} )
    public Resposta socialJaCadastrado( SocialJaCadastradoException e ){

        log.info("Social ja Cadastrado");

        return new Resposta( Notificacao.SOCIAL_JA_CADASTRADO, e.getMessage() );

    }

	@ResponseStatus( HttpStatus.NOT_FOUND )
    @ExceptionHandler( {SocialNaoEncontradoException.class} )
    public Resposta socialNaoEncontrado( SocialNaoEncontradoException e ){

        log.info("Social nao Encontrado");

        return new Resposta( Notificacao.SOCIAL_NAO_ENCONTRADO, e.getMessage() );

    }
	
	@ResponseStatus( HttpStatus.NOT_FOUND )
    @ExceptionHandler( {EnderecoNaoEncontradoException.class} )
    public Resposta enderecoNaoEncontrado( EnderecoNaoEncontradoException e ){

        log.info("Endereco nao Encontrado");

        return new Resposta( Notificacao.ENDERECO_NAO_ENCONTRADO, e.getMessage() );

    }
	
}
