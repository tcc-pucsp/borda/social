package br.com.projeto.pucsp.tcc.social.observador;

import br.com.projeto.pucsp.tcc.social.modelo.GeolocalizacaoRecebimento;

public interface Geolocalizacao {

	void processar( GeolocalizacaoRecebimento geolocalizacaoRecebimento );
	
}
