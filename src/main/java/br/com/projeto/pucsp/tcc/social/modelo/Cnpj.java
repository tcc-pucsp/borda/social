package br.com.projeto.pucsp.tcc.social.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.br.CNPJ;

import br.com.projeto.pucsp.tcc.social.dto.CnpjDto;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table( name = "cnpj" )
@Data
@NoArgsConstructor
public class Cnpj {

	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY )
	private Integer id;
	
	@Size( max = 14 )
	@NotBlank
	@CNPJ
	@Column( name = "registro", columnDefinition = "char" ) 
	private String registro;
	
	public Cnpj( CnpjDto cnpjDto ) {
		
		this.registro = cnpjDto.getRegistro();
		
	}
	
	public void atualizar( CnpjDto cnpjDto ) {
		
		this.registro = cnpjDto.getRegistro();
		
	}
	
}
