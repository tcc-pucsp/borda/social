package br.com.projeto.pucsp.tcc.social.excecao;

import br.com.projeto.pucsp.tcc.social.enumeracao.Notificacao;
import lombok.Getter;

@Getter
public class SocialJaCadastradoException extends RuntimeException {

	private static final long serialVersionUID = 6550477725682926573L;

	private final Notificacao notificacao;
	
	public SocialJaCadastradoException( Integer id ) {
		
		super( String.format("Social %d ja cadastrado", id) );
		
		this.notificacao = Notificacao.SOCIAL_JA_CADASTRADO;
		
	}
	
}
