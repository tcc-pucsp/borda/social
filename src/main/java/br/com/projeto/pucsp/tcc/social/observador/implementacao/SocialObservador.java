package br.com.projeto.pucsp.tcc.social.observador.implementacao;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import br.com.projeto.pucsp.tcc.social.modelo.GeolocalizacaoRecebimento;
import br.com.projeto.pucsp.tcc.social.observador.Geolocalizacao;
import br.com.projeto.pucsp.tcc.social.servico.ServicoGeolocalizacao;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class SocialObservador implements Geolocalizacao {
	
	private final ServicoGeolocalizacao servicoGeolocalizacao;
	
	public SocialObservador( ServicoGeolocalizacao servicoGeolocalizacao ) {
		
		this.servicoGeolocalizacao = servicoGeolocalizacao;
		
	}
	
    @RabbitListener(queues = "social.endereco")
    @Override
    public void processar( GeolocalizacaoRecebimento geolocalizacaoRecebimento ) {

        log.info("Processar geolocalizacao  : {}", geolocalizacaoRecebimento);

        servicoGeolocalizacao.persistirGeolocalizacao( geolocalizacaoRecebimento );

    }
	
}
