package br.com.projeto.pucsp.tcc.social.cliente;

import br.com.projeto.pucsp.tcc.social.modelo.EmailPublicacao;

public interface ClienteNotificacao {
	
	void publicar( EmailPublicacao emailPublicacao  );
	
}
