package br.com.projeto.pucsp.tcc.social.modelo;

import lombok.Value;

@Value
public class EmailPublicacao {

	String endereco;

    String mensagem;

    String assunto;
	
}
