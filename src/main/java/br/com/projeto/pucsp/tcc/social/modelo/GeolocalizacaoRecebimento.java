package br.com.projeto.pucsp.tcc.social.modelo;

import lombok.Data;

@Data
public class GeolocalizacaoRecebimento {

	private Integer id;
	
    private Double latitude;

    private Double longitude;

    private String endereco;
    
}
