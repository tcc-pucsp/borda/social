package br.com.projeto.pucsp.tcc.social.propriedade;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Data;

@Data
@ConfigurationProperties( prefix = "rabbit.notificacao" )
public class PropriedadeNotificacao {

	private String topicExchangeNotificacao;
	
}
