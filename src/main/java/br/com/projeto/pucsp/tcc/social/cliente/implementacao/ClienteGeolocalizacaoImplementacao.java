package br.com.projeto.pucsp.tcc.social.cliente.implementacao;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

import br.com.projeto.pucsp.tcc.social.cliente.ClienteGeolocalizacao;
import br.com.projeto.pucsp.tcc.social.modelo.EnderecoPublicacao;
import br.com.projeto.pucsp.tcc.social.propriedade.PropriedadeGeolocalizacao;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class ClienteGeolocalizacaoImplementacao implements ClienteGeolocalizacao {

	private final RabbitTemplate rabbitTemplate;
	
	private final PropriedadeGeolocalizacao propriedadeGeolocalizacao;
	
	public ClienteGeolocalizacaoImplementacao( RabbitTemplate rabbitTemplate, PropriedadeGeolocalizacao propriedadeGeolocalizacao ) {
		
		this.rabbitTemplate = rabbitTemplate;
		
		this.propriedadeGeolocalizacao = propriedadeGeolocalizacao;
		
	}
	
	@Override
	public void publicar( EnderecoPublicacao enderecoPublicacao ) {
		
		rabbitTemplate.convertAndSend( propriedadeGeolocalizacao.getTopicExchange(), enderecoPublicacao );
		
		log.info("Mensagem Publicada com Sucesso no RabbitMQ");
	}
	
}
