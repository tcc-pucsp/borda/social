package br.com.projeto.pucsp.tcc.social.servico;

import org.springframework.stereotype.Service;

import br.com.projeto.pucsp.tcc.social.excecao.SocialNaoEncontradoException;
import br.com.projeto.pucsp.tcc.social.proxy.LoginProxy;
import br.com.projeto.pucsp.tcc.social.recurso.LoginRecurso;
import br.com.projeto.pucsp.tcc.social.repositorio.RepositorioSocial;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ServicoLogin implements LoginRecurso{

	private final RepositorioSocial repositorio;
	
	public ServicoLogin( RepositorioSocial repositorio ) {
		
		this.repositorio = repositorio;
		
	}
	
	@Override
	public LoginProxy login( String cnpj ) {
		
		final LoginProxy loginProxy = repositorio.findSocialByCnpjRegistroAndDataEncerramentoIsNull( cnpj, LoginProxy.class )
				.orElseThrow( () -> new SocialNaoEncontradoException( "" ) );
		
		log.info("Login {} Permitido", loginProxy.getId() );
		
		return loginProxy;
		
	}
	
}
