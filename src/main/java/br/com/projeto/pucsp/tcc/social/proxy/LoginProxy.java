package br.com.projeto.pucsp.tcc.social.proxy;

public interface LoginProxy extends SocialProxy{
	
	String getSenha();

}
