package br.com.projeto.pucsp.tcc.social.servico.implementacao;

import org.springframework.stereotype.Service;

import br.com.projeto.pucsp.tcc.social.excecao.EnderecoNaoEncontradoException;
import br.com.projeto.pucsp.tcc.social.modelo.GeolocalizacaoRecebimento;
import br.com.projeto.pucsp.tcc.social.modelo.Social;
import br.com.projeto.pucsp.tcc.social.repositorio.RepositorioSocial;
import br.com.projeto.pucsp.tcc.social.servico.ServicoGeolocalizacao;
import br.com.projeto.pucsp.tcc.social.servico.ServicoNotificacao;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ServicoGeolocalizacaoImplementacao implements ServicoGeolocalizacao {

	private final RepositorioSocial repositorio;
	
	private final ServicoNotificacao servicoNotificacao;
	
	public ServicoGeolocalizacaoImplementacao( RepositorioSocial repositorioSocial,
								  			   ServicoNotificacao servicoNotificacao ) {
		
		this.repositorio = repositorioSocial;
		this.servicoNotificacao = servicoNotificacao;
		
	}
	
	@Override
	public void persistirGeolocalizacao( GeolocalizacaoRecebimento geolocalizacaoRecebimento ) {
		
		final Social social = repositorio.findSocialByEnderecoId( geolocalizacaoRecebimento.getId() ).orElseThrow( () -> 
			new EnderecoNaoEncontradoException( geolocalizacaoRecebimento.getId() )
		);
		
		social.getEndereco().atualizarGeolocalizacao( geolocalizacaoRecebimento );
		
		social.setAtivo( true );
		
		repositorio.save( social );
		
		servicoNotificacao.enviarEmail( social );
		
		log.info("Endereco com id {} persistido geolocalizacao", geolocalizacaoRecebimento.getId());
		
	}

}
