package br.com.projeto.pucsp.tcc.social.modelo;

import java.sql.Timestamp;
import java.time.LocalDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.data.annotation.LastModifiedDate;

import br.com.projeto.pucsp.tcc.social.dto.SocialDto;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "social")
@Data
@NoArgsConstructor
public class Social {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotBlank
    @Column(name = "nome_instituicao")
    private String nomeInstituicao;

    @NotBlank
    @Size(max = 15)
    private String telefone;

    @Email
    @Column(name = "email", columnDefinition = "char")
    private String email;

    @NotBlank
    @Column(name = "senha", columnDefinition = "char")
    private String senha;

    @NotNull
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "endereco_fk", referencedColumnName = "id")
    private Endereco endereco;

    @NotNull
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "cnpj_fk", referencedColumnName = "id")
    private Cnpj cnpj;
    
    private Boolean ativo;

    @Column(name = "data_encerramento")
    private LocalDateTime dataEncerramento;

    @LastModifiedDate
    @Column(name = "data_cadastro")
    private LocalDateTime dataCadastro;

    @LastModifiedDate
    @Column(name = "data_atualizacao")
    private Timestamp dataAtualizacao;

    public Social(SocialDto socialDto) {

        nomeInstituicao = socialDto.getNomeInstituicao();
        telefone = socialDto.getTelefone();
        email = socialDto.getEmail();
        senha = socialDto.getSenha();
        endereco = new Endereco(socialDto.getEndereco());
        cnpj = new Cnpj(socialDto.getCnpj());
        ativo = false;
        dataCadastro = LocalDateTime.now();
        dataAtualizacao = Timestamp.valueOf(LocalDateTime.now());

    }

    public void atualizar(SocialDto socialDto) {

        nomeInstituicao = socialDto.getNomeInstituicao();
        telefone = socialDto.getTelefone();
        email = socialDto.getEmail();
        senha = socialDto.getSenha();
        endereco.atualizar(socialDto.getEndereco());
        ativo = false;
        dataAtualizacao = Timestamp.valueOf(LocalDateTime.now());

    }

    private void atualizar(Social social) {

        nomeInstituicao = social.getNomeInstituicao();
        email = social.getEmail();
        senha = social.getSenha();
        endereco.atualizar(social.getEndereco());
        ativo = false;
        dataAtualizacao = Timestamp.valueOf(LocalDateTime.now());

    }

    public boolean encerrado() {
        return dataEncerramento != null;
    }


    public void reativar(Social social) {
        atualizar(social);
        dataEncerramento = null;
    }

    public void encerrar() {
        dataEncerramento = LocalDateTime.now();
    }

}
