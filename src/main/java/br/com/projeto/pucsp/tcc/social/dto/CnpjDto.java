package br.com.projeto.pucsp.tcc.social.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.br.CNPJ;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public final class CnpjDto {

	@Size( max = 14 )
	@NotBlank
	@CNPJ
	private final String registro;
	
}
