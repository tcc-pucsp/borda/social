package br.com.projeto.pucsp.tcc.social.excecao;

import br.com.projeto.pucsp.tcc.social.enumeracao.Notificacao;
import lombok.Getter;

@Getter 
public class EnderecoNaoEncontradoException extends RuntimeException{

	private static final long serialVersionUID = 1839286415441109726L;

	private final Notificacao notificacao;

	public EnderecoNaoEncontradoException( Integer id ) {

		super( String.format("Endereco %d nao encontrado", id) );

		this.notificacao = Notificacao.ENDERECO_NAO_ENCONTRADO;

	}

}
