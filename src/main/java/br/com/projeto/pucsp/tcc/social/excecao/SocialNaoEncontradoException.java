package br.com.projeto.pucsp.tcc.social.excecao;

import br.com.projeto.pucsp.tcc.social.enumeracao.Notificacao;
import lombok.Getter;

@Getter
public class SocialNaoEncontradoException extends RuntimeException{

	private static final long serialVersionUID = 6517257005268000102L;
	
	private final Notificacao notificacao;
	
	public SocialNaoEncontradoException( String mensagem ) {
		
		super( String.format("Social %s nao encontrado", mensagem) );
		
		this.notificacao = Notificacao.SOCIAL_NAO_ENCONTRADO;
		
	}
	
	public SocialNaoEncontradoException( Integer id ) {
		
		super( String.format("Social %d nao foi encontrado", id) );
		
		this.notificacao = Notificacao.SOCIAL_NAO_ENCONTRADO;
		
	}
	
}
