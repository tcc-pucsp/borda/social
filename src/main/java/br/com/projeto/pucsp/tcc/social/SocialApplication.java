package br.com.projeto.pucsp.tcc.social;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import br.com.projeto.pucsp.tcc.social.propriedade.PropriedadeGeolocalizacao;
import br.com.projeto.pucsp.tcc.social.propriedade.PropriedadeNotificacao;

@SpringBootApplication
@EnableConfigurationProperties({PropriedadeGeolocalizacao.class, PropriedadeNotificacao.class})
public class SocialApplication {

	public static void main(String[] args) {
		SpringApplication.run(SocialApplication.class, args);
	}

}
