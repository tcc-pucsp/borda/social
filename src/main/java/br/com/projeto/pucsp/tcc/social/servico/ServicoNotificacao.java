package br.com.projeto.pucsp.tcc.social.servico;

import org.springframework.stereotype.Service;

import br.com.projeto.pucsp.tcc.social.cliente.ClienteNotificacao;
import br.com.projeto.pucsp.tcc.social.modelo.EmailPublicacao;
import br.com.projeto.pucsp.tcc.social.modelo.Social;

@Service
public class ServicoNotificacao {

	private final ClienteNotificacao clienteNotificacao;
	
	public ServicoNotificacao( ClienteNotificacao clienteNotificacao ) {
		this.clienteNotificacao = clienteNotificacao;
	}
	
	public void enviarEmail( Social social ) {
		
		String endereco = social.getEmail();
		String mensagem = "Ola " + social.getNomeInstituicao() + ",\nVoce Foi Cadastrado com Sucesso.\nAgora voce pode fazer login em nosso sistema.\n\natt, FilantroFlex";
		String assunto = "Cadastro Social " + social.getNomeInstituicao();
		
		EmailPublicacao emailPublicacao = new EmailPublicacao(endereco, mensagem, assunto);
		
		clienteNotificacao.publicar( emailPublicacao );
		
	}	
	
}
