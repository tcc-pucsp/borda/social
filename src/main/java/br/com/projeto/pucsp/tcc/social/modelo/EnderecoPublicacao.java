package br.com.projeto.pucsp.tcc.social.modelo;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class EnderecoPublicacao {

	private Integer id;
	
	private String cep;
	
	private String logradouro;
	
	private Integer numero;
	
	private String cidade;
	
	private String estado;
	
	public EnderecoPublicacao( Endereco endereco ) {
		
		id = endereco.getId();
		cep = endereco.getCep();
		logradouro = endereco.getLogradouro();
		numero = endereco.getNumero();
		cidade = endereco.getCidade();
		estado = endereco.getEstado();
		
	}
	
}
