package br.com.projeto.pucsp.tcc.social.modelo;

import java.sql.Timestamp;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.data.annotation.LastModifiedDate;

import br.com.projeto.pucsp.tcc.social.dto.EnderecoDto;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table( name = "endereco" )
@Data
@NoArgsConstructor
public class Endereco {

	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY )
	private Integer id;
	
	@Size( max = 10 )
	@NotBlank
	@Column( name = "cep", columnDefinition = "char" ) 
	private String cep;
	
	@Size( max = 255 )
	@Column( name = "logradouro" ) 
	private String logradouro;
	
	@Size( max = 30 )
	@Column( name = "complemento" ) 
	private String complemento;
	
	@NotNull
	@Column( name = "numero" ) 
	private Integer numero;
	
	@NotBlank
	@Column( name = "cidade" )
	private String cidade;
	
	@NotBlank
	@Column( name = "estado" )
	private String estado;
	
	@Column( name = "latitude", columnDefinition = "decimal" ) 
	private Double latitude;
	
	@Column( name = "longitude", columnDefinition = "decimal" ) 
	private Double longitude;
	
	@LastModifiedDate
	@Column( name = "data_cadastro" ) 
	private LocalDateTime dataCadastro;
	
	@LastModifiedDate
	@Column( name = "data_atualizacao" )
	private Timestamp dataAtualizacao;
	
	public Endereco( EnderecoDto enderecoDto ) {
		
		cep = formatarCep( enderecoDto.getCep() );
		logradouro = enderecoDto.getLogradouro();
		complemento = enderecoDto.getComplemento();
		numero = enderecoDto.getNumero();
		cidade = enderecoDto.getCidade();
		estado = enderecoDto.getEstado();
		dataCadastro = LocalDateTime.now();
		dataAtualizacao = Timestamp.valueOf( LocalDateTime.now() );
		
	}
	
	public void atualizar( EnderecoDto enderecoDto ) {
		
		cep = formatarCep( enderecoDto.getCep() );
		logradouro = enderecoDto.getLogradouro();
		complemento = enderecoDto.getComplemento();
		numero = enderecoDto.getNumero();
		cidade = enderecoDto.getCidade();
		estado = enderecoDto.getEstado();
		dataAtualizacao = Timestamp.valueOf( LocalDateTime.now() );
		
	}
	
	public void atualizar( Endereco endereco ) {
		
		cep = formatarCep( endereco.getCep() );
		logradouro = endereco.getLogradouro();
		complemento = endereco.getComplemento();
		numero = endereco.getNumero();
		cidade = endereco.getCidade();
		estado = endereco.getEstado();
		
	}
	
	public void atualizarGeolocalizacao( GeolocalizacaoRecebimento geolocalizacaoRecebimento ) {
		
		this.latitude = geolocalizacaoRecebimento.getLatitude();
		this.longitude = geolocalizacaoRecebimento.getLongitude();
		
	}
	
	private String formatarCep( String cep ) {
		
		String cepLimpo = cep.replace("-", "").replace(".", "");
		
		return cepLimpo.substring( 0, 5 )
				.concat( "-" )
				.concat( cepLimpo.substring( 5, cepLimpo.length() ) );

	}
	
}
