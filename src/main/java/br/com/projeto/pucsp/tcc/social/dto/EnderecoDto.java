package br.com.projeto.pucsp.tcc.social.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public final class EnderecoDto {
	
	@Size( max = 10 )
	@NotBlank
	private final String cep;
	
	@Size( max = 255 )
	private final String logradouro;
	
	@Size( max = 30 )
	private final String complemento;
	
	@NotNull
	private final Integer numero;
	
	@NotBlank
	private final String cidade;
	
	@NotBlank
	private final String estado;

}
