package br.com.projeto.pucsp.tcc.social.recurso;

import br.com.projeto.pucsp.tcc.social.dto.SocialDto;
import br.com.projeto.pucsp.tcc.social.modelo.IdSocial;
import br.com.projeto.pucsp.tcc.social.proxy.SocialProxy;

public interface SocialRecurso {

	IdSocial cadastrarSocial( SocialDto socialDto );

	SocialProxy obterSocial( String cnpj );

	IdSocial encerrarSocial( Integer id );

	IdSocial atualizarSocial( Integer id, SocialDto socialDto );

}
