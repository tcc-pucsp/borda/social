package br.com.projeto.pucsp.tcc.social.modelo;

import br.com.projeto.pucsp.tcc.social.enumeracao.Notificacao;
import lombok.Value;

@Value
public class Resposta {

	Notificacao notificacao;

	String mensagem;

}
