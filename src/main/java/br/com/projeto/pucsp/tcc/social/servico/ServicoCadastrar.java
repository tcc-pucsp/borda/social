package br.com.projeto.pucsp.tcc.social.servico;

import java.util.Optional;

import org.springframework.stereotype.Service;

import br.com.projeto.pucsp.tcc.social.dto.SocialDto;
import br.com.projeto.pucsp.tcc.social.excecao.SocialJaCadastradoException;
import br.com.projeto.pucsp.tcc.social.modelo.Social;
import br.com.projeto.pucsp.tcc.social.repositorio.RepositorioSocial;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ServicoCadastrar {

	private final RepositorioSocial repositorio;
	
	public ServicoCadastrar( RepositorioSocial repositorioSocial ) {
		repositorio = repositorioSocial;
	}
	
	public Social processar( SocialDto socialDto ){
		
		Social social = new Social( socialDto );
		
		Optional<Social> socialCadastrado = repositorio.findSocialByCnpjRegistro( social.getCnpj().getRegistro() );
		
		if( socialCadastrado.isEmpty() ) {
			return cadastrar( social );
		}
		
		return reativar( social, socialCadastrado.get() );
		
	}
	
	private Social cadastrar( Social social ){
		
		Social socialCadastrado = repositorio.save( social );
		
		log.info("Social {} cadastrado", socialCadastrado.getId() );
		
		return socialCadastrado;
	
	}
	
	private Social reativar( Social social, Social socialCadastrado ){
		
		if( !socialCadastrado.encerrado() ) {
			throw new SocialJaCadastradoException( socialCadastrado.getId() );			
		}
		
		socialCadastrado.reativar( social );
		
		log.info("Social {} reativado", socialCadastrado.getId() );
		
		return repositorio.save( socialCadastrado );		
		
	}
	
}
