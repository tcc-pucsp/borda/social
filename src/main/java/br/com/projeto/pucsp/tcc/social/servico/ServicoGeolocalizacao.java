package br.com.projeto.pucsp.tcc.social.servico;

import br.com.projeto.pucsp.tcc.social.modelo.GeolocalizacaoRecebimento;

public interface ServicoGeolocalizacao {

	void persistirGeolocalizacao( GeolocalizacaoRecebimento geolocalizacaoRecebimento );
	
}
