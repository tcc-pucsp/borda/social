package br.com.projeto.pucsp.tcc.social.cliente;

import br.com.projeto.pucsp.tcc.social.modelo.EnderecoPublicacao;

public interface ClienteGeolocalizacao {
	
	void publicar( EnderecoPublicacao enderecoPublicacao  );

}
