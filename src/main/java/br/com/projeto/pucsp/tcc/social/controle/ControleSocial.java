package br.com.projeto.pucsp.tcc.social.controle;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.projeto.pucsp.tcc.social.dto.SocialDto;
import br.com.projeto.pucsp.tcc.social.modelo.IdSocial;
import br.com.projeto.pucsp.tcc.social.proxy.SocialProxy;
import br.com.projeto.pucsp.tcc.social.recurso.SocialRecurso;

@RequestMapping( path = "/social")
@RestController
public class ControleSocial implements SocialRecurso {

	private final SocialRecurso servico;
	
	public ControleSocial( SocialRecurso servico ) {
		
		this.servico = servico;
		
	}
	
	@ResponseStatus( HttpStatus.CREATED )
	@PostMapping
	@Override
	public @ResponseBody IdSocial cadastrarSocial( @Valid @RequestBody SocialDto socialDto ) {
		
		return servico.cadastrarSocial( socialDto );
		
	}
	
	@GetMapping( path = "/{cnpj}")
	@Override
	public @ResponseBody SocialProxy obterSocial( @PathVariable("cnpj") String cnpj ) {
		
		return servico.obterSocial( cnpj );
		
	}
	
	@PutMapping( path = "/{id}")
	@Override
	public @ResponseBody IdSocial atualizarSocial( @PathVariable("id") Integer id, @Valid @RequestBody SocialDto socialDto ) {
		
		return servico.atualizarSocial( id, socialDto );
		
	}
	
	@DeleteMapping( path = "/{id}")
	@Override
	public @ResponseBody IdSocial encerrarSocial( @PathVariable("id") Integer id ) {
		
		return servico.encerrarSocial( id );
		
	}
	
}
