package br.com.projeto.pucsp.tcc.social.controle;

import javax.validation.Valid;

import org.hibernate.validator.constraints.br.CNPJ;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.projeto.pucsp.tcc.social.proxy.LoginProxy;
import br.com.projeto.pucsp.tcc.social.recurso.LoginRecurso;

@RequestMapping( path = "/social/login")
@RestController
public class ControleLogin implements LoginRecurso{

	private final LoginRecurso servico;
	
	public ControleLogin( LoginRecurso servico ) {
		
		this.servico = servico;
		
	}
	
	@ResponseStatus( HttpStatus.OK )
	@GetMapping("/{cnpj}")
	@Override
	public @ResponseBody LoginProxy login( @Valid @CNPJ @PathVariable("cnpj") String cnpj ) {
		
		return servico.login( cnpj );
		
	}
	
}
